#
# Copyright 2018-2024 Fragment Contributors
# SPDX-License-Identifier: Apache-2.0
#
from fragment.app.cli.cli import main

if __name__ == "__main__":
    main()
