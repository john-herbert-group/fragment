#
# Copyright 2018-2024 Fragment Contributors
# SPDX-License-Identifier: Apache-2.0
#
__all__ = ["main"]
from .cli import fragment as main