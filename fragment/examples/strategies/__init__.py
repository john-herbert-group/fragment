#
# Copyright 2018-2024 Fragment Contributors
# SPDX-License-Identifier: Apache-2.0
#
from pkg_resources import resource_filename

EXAMPLE_STRATEGY_PATH = resource_filename(__name__, "example.yaml")
WATER6_STRATEGY_PATH = resource_filename(__name__, "water6_test.yaml")
