#
# Copyright 2018-2024 Fragment Contributors
# SPDX-License-Identifier: Apache-2.0
#
from pkg_resources import resource_filename

# FRAGMENT FILES
F_WATER10_FRAG_PATH = resource_filename(__name__, "Fw10.frag")
WATER6_FRAG_PATH = resource_filename(__name__, "water6.frag")

# PDB FILES
SMALL_PROTIEN_SYS_PATH = resource_filename(__name__, "small_protien.pdb")

# XYZ FILES
ACETYLENE_SYS_PATH = resource_filename(__name__, "acetylene.xyz")
BENZENE_SYS_PATH = resource_filename(__name__, "benzene.xyz")
ETHANE_SYS_PATH = resource_filename(__name__, "ethane.xyz")
ETHANOL_SYS_PATH = resource_filename(__name__, "ethanol.xyz")
ETHENE_SYS_PATH = resource_filename(__name__, "ethene.xyz")
FORMALDEHYDE_SYS_PATH = resource_filename(__name__, "formaldehyde.xyz")
MANY_BENZENE_SYS_PATH = resource_filename(__name__, "many_benzene.xyz")
MG_COMPLEX_SYS_PATH = resource_filename(__name__, "mg_complex.xyz")
MG_ALDEHYDE = resource_filename(__name__, "MgOCH2.xyz")
TWO_AA_SYS_PATH = resource_filename(__name__, "two_AA.xyz")
SIX_BENZENE_SYS_PATH = resource_filename(__name__, "6-benzene.xyz")
WATER6_SYS_PATH = resource_filename(__name__, "water6.xyz")
