.. Fragment documentation master file, created by
   sphinx-quickstart on Fri Jul 16 14:54:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fragme∩t's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   user_guide/index
   developer_guide/index
   api_reference/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
