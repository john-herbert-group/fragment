#
# Copyright 2018-2024 Fragment Contributors
# SPDX-License-Identifier: Apache-2.0
#
from conformer_core.testing import DBTestCase


class FragmentTestCase(DBTestCase):
    CONFIG = "fragment"
