<!--
Copyright 2018-2024 Fragment Contributors
SPDX-License-Identifier: Apache-2.0
-->
# Fragme∩t 🔨🪞

Fragme∩t is a framework for fragmentation. The package was designed make it easy to prototype, implement, and benchmark fragmentation methods. The code is currently in a pre-release state so be aware that features and backwards compatibility are subject to change.

## Quick-Start

See the [quick-start](https://fragment-qc.gitlab.io/fragment/user_guide/getting_started.html) guide for brief example of using Fragme∩t.

## Documentation

👷 The project documentation can be found [here](https://fragment-qc.gitlab.io/fragment). 

## Publications using Fragme∩t

[Fragment-based calculations of enzymatic thermochemistry require dielectric boundary conditions](https://doi.org/10.1021/acs.jpclett.3c00533). P. E. Bowling, D. R. Broderick, and J. M. Herbert. J. Phys. Chem. Lett. __14__, 3826–3834 (2023).

[Scalable generalized screening for high-order terms in the many-body expansion: Algorithm, open-source implementation, and demonstration](https://doi.org/10.1063/5.0174293).
D. R. Broderick and J. M. Herbert. J. Chem. Phys. __159__, 174801:1–12 (2023)